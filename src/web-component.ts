export default class WebComponent extends HTMLElement {
    static formAssociated = false;

    public static TAG_NAME = "web-component";
    public static CLASS = WebComponent;
    public static observedAttributes = ["name", "value"];

    protected attr: { [key: string]: any } = {};
    
    private rendered: boolean = false;
    private shadow: ShadowRoot;
    private internals: ElementInternals;
    private observer: MutationObserver | undefined;

    // create a MutationObserver to monitor attribute changes
    // https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
    private observerConfig: MutationObserverInit = {
        attributes: true,  // mutations to target’s attributes are to be observed
        childList: true,  // mutations to target’s children are to be observed.
        subtree: true,    // not just target, but also target’s descendants are to be observed.
    };

    /**
     * Template literal for the component's content
     * 
     * @see {@link "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals"}
     * 
     * @private
     * @memberof HelloWorld
     * @property {string} html - HTML template literal
     * @property {string} css - CSS template literal
     */
    protected template = {
        html: `<div class="someClass">$name: $value</div>`,
        css: `.someClass { color: green; }`,
    }

    private observerSetup() {
        if (this.observer) this.observer.disconnect();
        let cfg = this.observerConfig;

        // new mutation observer
        this.observer = new MutationObserver(
            (mutations: MutationRecord[], observer: MutationObserver) => {
                this.onAttributeChange(mutations, observer);
            });
        // observe the shadow root
        this.observer.observe(this, cfg);
    }

    /**
     * event handler for attribute changes
     */
    private onAttributeChange(mutations: MutationRecord[], observer: MutationObserver) {
        console.log("onAttributeChange");
        for (const mutation of mutations) {
            if (mutation.type !== 'attributes') continue;
            let newVal = '';
            if (mutation.attributeName === 'value')
                newVal = (mutation.target as HTMLElement).getAttribute(mutation.attributeName) as string;
            console.log(mutations);
            this.dispatchAttrubuteChangeEvent(mutation.attributeName ?? '', mutation.oldValue, newVal);
        }
    }

    private createAttributeChangeEvent(name: string, oldValue: any, newValue: any): CustomEvent {
        return new CustomEvent('change', {
            bubbles: true,
            composed: true,
            cancelable: true,
            detail: {
                name: name,
                oldValue: oldValue,
                newValue: newValue,
            }
        });
    }

    private dispatchAttrubuteChangeEvent(name: string, oldValue: any, newValue: any): void {
        // emit change event
        let evt = this.createAttributeChangeEvent(name, oldValue, newValue)
        console.log("dispatching change event")
        console.log(evt);
        this.dispatchEvent(evt);
    }

    constructor() {
        super();
        this.shadow = this.attachShadow({ mode: "closed" });

        // expose form fields
        this.internals = this.attachInternals();

        // setup observer
        this.observerSetup();
    }

    private render() {
        let content = this.template.html;
        for (let key in this.attr) {
            content = content.replace(`$${key}`, this.attr[key]);
        }
        this.rendered = true;
        this.shadow.innerHTML = "<style>" + this.template.css + "</style>" + content;
    }

    connectedCallback() {
        this.render();
        // emit first attribute values
        for (let key in this.attr) {
            this.dispatchAttrubuteChangeEvent(key, undefined, this.attr[key]);
        }
    }

    /** */
    attributeChangedCallback(name: string, oldValue: any, newValue: any) {
        console.log(`Attribute ${name} changed from «${oldValue}» to «${newValue}»`);
        this.attr[name] = newValue;
        if (name == "value") {
            this.internals.setFormValue(newValue);
        }
        this.render();
    }

    /**
     * register the tag
     * 
     * This method has to be run once so the browser knows of the custom tag. 
     * This needs to be done very early in the application.
     * 
     * @param tagName custom tag name
     * @returns void
     */
    static define(tagName: string = this.TAG_NAME): void {
        if (customElements.get(tagName)) return;

        if (this.CLASS.observedAttributes.indexOf("value") > -1 && this.CLASS.observedAttributes.indexOf("name") > -1)
            this.CLASS.formAssociated = true;

        console.log("registering " + tagName);
        customElements.define(tagName, this.CLASS);

    }

    // Form controls usually expose a "value" property
    get value(): any { return this.attr.value; }
    set value(v: any) { this.attr.value = v; }

    // The following properties and methods aren't strictly required,
    // but browser-level form controls provide them. Providing them helps
    // ensure consistency with browser-provided controls.
    get form(): HTMLFormElement | null { return this.internals.form; }
    get name(): any | null { return this.getAttribute('name'); }
    get type(): string { return this.localName; }
    get validity(): ValidityState { return this.internals.validity; }
    get validationMessage(): string { return this.internals.validationMessage; }
    get willValidate(): boolean { return this.internals.willValidate; }

    checkValidity(): boolean { return this.internals.checkValidity(); }
    reportValidity(): boolean { return this.internals.reportValidity(); }
}
