import WebComponent from "./web-component";

/**
 * Component2
 * 
 * This is an example how to extend the WebComponent class with a seperate template.
 * 
 * @export
 * @class Component2
 * @extends {WebComponent}
 */
export default class Component2 extends WebComponent {
    public static override TAG_NAME = "web-cpm2";
    public static override CLASS = Component2;
    // add new observed attribztes here
    //public static observedAttributes = ["name", "value"];

    protected override template = {
        html: `<div class="someClass"><b>$name</b>: $value</div>`,
        css: `.someClass { color: red; }`,
    }

    constructor() {
        super();
    }
}