import WebComponent from './web-component';
import Component2 from './component2';
import { format } from './formatter';

export function main() {
    WebComponent.define(WebComponent.TAG_NAME);
    Component2.define(Component2.TAG_NAME);

    // test the formatter
    let name = "test";
    let value = "test value";
    let result = format`some text {name} after {value}`;
    console.log(result);
}

main();