/**
 * Template formatter
 * 
 * This method renders template literals in a deferred way. 
 * It is used to render the component's content if prefixed to the 
 * template literal.
 * 
 * @see {@link "https://stackoverflow.com/questions/22607806/defer-execution-for-es6-template-literals#answer-54129531"}
 * @param {any[]} param0 
 * @param  {...any} tags 
 * @returns 
 */
export default function formatter([first, ...rest]: TemplateStringsArray[], ...tags: any[]) {
    return (values: any) => rest.reduce((acc, curr, i) => {
        return acc + values[tags[i]] + curr;
    }, first);
}

export function format(strings: TemplateStringsArray, ...values: string[]) {
    console.log("format", strings, values);
    return "formatted";
}